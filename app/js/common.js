$(document).ready(function () {
    $("#hamburger").click(function () {
        $(this).toggleClass("is-active");
    });

    $(".owl-carousel").owlCarousel({
        loop: true,
        items: 5,
        margin: 45,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            1300: {
                items: 4
            },
            1601: {
                items: 5
            }
        },
    });

    $('.js-example-basic-single').select2({
        minimumResultsForSearch: Infinity,
        width: '245px',
    });
});
